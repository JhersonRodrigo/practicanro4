/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.practican4;

/**
 *
 * @author Jherson Rodrigo Mamani Poma
 */
public class cola {
    private pila p1, p2;
    
    public cola(){
        p1= new pila();
        p2= new pila();
    }
    
    public void introducirDatos(int dato){
        while (!p1.isEmpty()){
            p2.push(p1.pop());
        }
    }
    
    public boolean vacio(){
        return (p1.isEmpty());
    }
    
    public int sacarDato(){
        int temp = -1;
        if (this.vacio())
            System.out.println("Cola vacia");
        else
            temp = p1.pop();
        return temp;
    }
}
